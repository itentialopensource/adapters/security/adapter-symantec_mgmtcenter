# Symantec_mgmtcenter

Vendor: Symantec
Homepage: https://www.broadcom.com/products/cybersecurity

Product: Management Center
Product Page: https://techdocs.broadcom.com/us/en/symantec-security-software/web-and-network-security/management-center/3-1.html

## Introduction
We classify Symantec Management Center into the Security (SASE) domain as it provides centralized management for security policies, compliance and configuration.

## Why Integrate
The Symantec Management Center adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Symantec Management Center. With this adapter you have the ability to perform operations such as:

- Security Group updates
- Security Rule Removal
- Security Rule Creation
- Run network compliance checks and remediation
- Determine configuration drift
- Device Onboarding
- Software Upgrade
- Firewall Configuration backup or restore

## Additional Product Documentation
The [Symantec Management Center REST API Reference](https://techdocs.broadcom.com/us/en/symantec-security-software/web-and-network-security/management-center/3-3/api.html)