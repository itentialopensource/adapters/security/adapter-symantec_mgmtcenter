
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:29PM

See merge request itentialopensource/adapters/adapter-symantec_mgmtcenter!11

---

## 0.4.3 [09-18-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-symantec_mgmtcenter!9

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:41PM

See merge request itentialopensource/adapters/adapter-symantec_mgmtcenter!8

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_19:53PM

See merge request itentialopensource/adapters/adapter-symantec_mgmtcenter!7

---

## 0.4.0 [05-15-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/security/adapter-symantec_mgmtcenter!6

---

## 0.3.3 [03-27-2024]

* Changes made at 2024.03.27_13:44PM

See merge request itentialopensource/adapters/security/adapter-symantec_mgmtcenter!5

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_16:15PM

See merge request itentialopensource/adapters/security/adapter-symantec_mgmtcenter!4

---

## 0.3.1 [02-27-2024]

* Changes made at 2024.02.27_11:49AM

See merge request itentialopensource/adapters/security/adapter-symantec_mgmtcenter!3

---

## 0.3.0 [01-02-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-symantec_mgmtcenter!2

---

## 0.2.0 [06-02-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/security/adapter-symantec_mgmtcenter!1

---

## 0.1.1 [07-12-2021]

- Initial Commit

See commit 332cb3d

---
